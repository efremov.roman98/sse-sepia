#ifndef SEPIA_H
#define SEPIA_H

extern void sepia_img(struct image *img);

extern void sse_sepia_img(struct image *img);

#endif
