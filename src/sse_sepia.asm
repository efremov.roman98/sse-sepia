%define pixels rdi
%define PIXEL_COMPONENT_SIZE 1
%define PIXEL_SIZE 3 * PIXEL_COMPONENT_SIZE
%define PIXEL(i) (pixels + (i) * PIXEL_SIZE)
%define BLUE(p) PIXEL(p)
%define GREEN(p) PIXEL(p) + PIXEL_COMPONENT_SIZE
%define RED(p) PIXEL(p) + 2 * PIXEL_COMPONENT_SIZE
%define MAX_PIXEL_VALUE 255
%define PACK_SZ 16


%macro calculate_result 0
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2
    mulps  xmm0, xmm3
    mulps  xmm1, xmm4
    mulps  xmm2, xmm5
    addps  xmm0, xmm1
    addps  xmm0, xmm2
    cvtps2dq xmm0, xmm0
    pminsd xmm0, xmm6
%endmacro

%macro clear_xmm012 0
    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2
%endmacro

%macro mov_sepia_coefficients 1
    movaps xmm3, [rsi + PACK_SZ * 3 * %1]
    movaps xmm4, [rsi + PACK_SZ * (3 * %1 + 1)]
    movaps xmm5, [rsi + PACK_SZ * (3 * %1 + 2)]
%endmacro

%macro insert_pixel_values 1
    pinsrb xmm0, byte[BLUE(%1)], 0
    pinsrb xmm0, byte[BLUE(%1 + 1)], 4
    pinsrb xmm1, byte[GREEN(%1)], 0
    pinsrb xmm1, byte[GREEN(%1 + 1)], 4
    pinsrb xmm2, byte[RED(%1)], 0
    pinsrb xmm2, byte[RED(%1 + 1)], 4
%endmacro

%macro shufps_xmm012_with 1
    shufps xmm0, xmm0, %1
    shufps xmm1, xmm1, %1
    shufps xmm2, xmm2, %1
%endmacro

section .text

global crutch_sse_sepia_4

; rdi - pointer to 4 source pixels
crutch_sse_sepia_4:

    ; init max pixel values for saturation
    pxor xmm6, xmm6
    mov eax, MAX_PIXEL_VALUE
    pinsrb xmm6, eax, 0
    shufps xmm6, xmm6, 0b00000000 ; xmm6 = 255|255|255|255

; #0
; xmm0 = b1|b1|b1|b2
; xmm1 = g1|g1|g1|g2
; xmm2 = r1|r1|r1|r2
; ---->  b1|g1|r1|b2
    clear_xmm012
    insert_pixel_values 0
    shufps_xmm012_with 0b00000001
    mov_sepia_coefficients 0
    calculate_result
    pextrb byte [BLUE(0)], xmm0, 0
    pextrb byte [GREEN(0)], xmm0, 4
    pextrb byte [RED(0)], xmm0, 8
    mov al, byte [BLUE(1)]
    pextrb byte [BLUE(1)], xmm0, 12
    
; #1
; xmm0 = b2|b2|b3|b3
; xmm1 = g2|g2|g3|g3
; xmm2 = r2|r2|r3|r3
; ---->  g2|r2|b3|g3
    clear_xmm012
    insert_pixel_values 1
    pinsrb xmm0, al, 0
    shufps_xmm012_with 0b00000101
    mov_sepia_coefficients 1
    calculate_result

    pextrb byte [GREEN(1)], xmm0, 0
    pextrb byte [RED(1)], xmm0, 4
    mov al, byte [BLUE(2)]
    pextrb byte [BLUE(2)], xmm0, 8
    mov dl, byte [GREEN(2)]
    pextrb byte [GREEN(2)], xmm0, 12

; #2
; xmm0 = b3|b4|b4|b4
; xmm1 = g3|g4|g4|g4
; xmm2 = r3|r4|r4|r4
; ---->  r3|b4|g4|r4
    clear_xmm012
    insert_pixel_values 2
    pinsrb xmm0, al, 0
    pinsrb xmm1, dl, 0
    shufps_xmm012_with 0b00010101
    mov_sepia_coefficients 2
    calculate_result

    pextrb byte [RED(2)], xmm0, 0
    pextrb byte [BLUE(3)], xmm0, 4
    pextrb byte [GREEN(3)], xmm0, 8
    pextrb byte [RED(3)], xmm0, 12

    ret
