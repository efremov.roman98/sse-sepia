/*
 * This is an extended set of functions for working with images.
 */

#ifndef IMAGE_EXT_H
#define IMAGE_EXT_H

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

extern struct pixel *pixel_of(const struct image *const, uint64_t x, uint64_t y);

#endif
