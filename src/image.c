#include <stdint.h>
#include <unitypes.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "image.h"
#include "image_ext.h"

#define BMP_MARKER 0x4D42


struct __attribute__((packed))
bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static enum read_status from_bmp(FILE *const file, struct image **img) {
    struct bmp_header header;
    uint32_t data_width;
    uint64_t trash;
    size_t trash_size;
    size_t i = 0;

    if (fread(&header, sizeof(struct bmp_header), 1, file) != 1)
        return READ_INVALID_HEADER;
    if (header.bfType != BMP_MARKER)
        return READ_INVALID_HEADER;
    if (header.biBitCount != sizeof(struct pixel) * 8)
        return READ_UNSUPPORTED_FORMAT;
    if (fseek((FILE *) file, header.bOffBits, SEEK_SET) != 0)
        return READ_INVALID_BITS;

    *img = malloc(sizeof(struct image));
    (*img)->width = header.biWidth;
    data_width = header.biWidth * sizeof(struct pixel);
    trash_size = (4 - (data_width % 4)) % 4;
    (*img)->height = header.biHeight;
    (*img)->data = malloc(data_width * (*img)->height);


    for (i = 0; i < (*img)->height; i++) {
        if (fread((*img)->data + i * (*img)->width, data_width, 1, file) != 1)
            return READ_INVALID_BITS;
        if (trash_size != 0 && fread(&trash, trash_size, 1, file) != 1)
            return READ_INVALID_BITS;
    }
    return READ_OK;
}

static enum write_status to_bmp(FILE *const file, struct image *const img) {
    struct bmp_header header;
    uint64_t data_width = img->width * sizeof(struct pixel);
    uint64_t offset = sizeof(header);
    uint64_t trash = 0;
    size_t trash_size = (4 - (data_width % 4)) % 4;
    uint64_t file_size = img->height * (data_width + trash_size) + offset;
    size_t i;


    if (img->height > UINT32_MAX || img->width > UINT32_MAX || file_size > UINT32_MAX)
        return WRITE_FAILED_TO_CONVERT;

    header.bfType = BMP_MARKER;
    header.bfileSize = (uint32_t) file_size;
    header.bfReserved = 0;
    header.bOffBits = (uint32_t) offset;
    header.biSize = 40;  /* size of BITMAPINFOHEADER structure, must be 40 */
    header.biWidth = (uint32_t) img->width;
    header.biHeight = (uint32_t) img->height;
    header.biPlanes = 1;
    header.biBitCount = sizeof(struct pixel) * 8;
    header.biCompression = 0;
    header.biSizeImage = (uint32_t) (file_size - offset);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (fwrite(&header, sizeof(header), 1, file) != 1)
        return WRITE_FAILED_TO_WRITE;
    for (i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, data_width, 1, file) != 1)
            return WRITE_FAILED_TO_WRITE;
        if (trash_size != 0 && fwrite(&trash, trash_size, 1, file) != 1)
            return WRITE_FAILED_TO_WRITE;
    }
    return WRITE_OK;
}

enum read_status image_open(const char *const filename, struct image **const img) {
    enum read_status status;
    uint16_t format_marker;
    FILE *file;

    if (NULL == (file = fopen(filename, "rb")))
        return READ_FAILED_TO_OPEN_FILE;

    if (fread(&format_marker, sizeof(format_marker), 1, file) != 1) {
        fclose(file);
        return READ_INVALID_HEADER;
    }

    rewind(file);
    if (BMP_MARKER == format_marker) {
        status = from_bmp(file, img);
        if (status != READ_OK) {
            fclose(file);
            return status;
        }
    } else {
        fclose(file);
        return READ_UNSUPPORTED_FORMAT;
    }

    if (fclose(file) != 0)
        return READ_FAILED_TO_CLOSE_FILE;
    return READ_OK;
}

enum write_status image_write(const char *const filename, struct image *const img, enum image_format format) {
    enum write_status status;
    FILE *file;

    if (NULL == (file = fopen(filename, "wb")))
        return WRITE_FAILED_TO_OPEN_FILE;

    if (BMP == format) {
        status = to_bmp(file, img);
    } else {
        fclose(file);
        return WRITE_UNSUPPORTED_FORMAT;
    }
    if (WRITE_OK != status) {
        fclose(file);
        return status;
    }

    if (fclose(file) != 0)
        return WRITE_FAILED_TO_CLOSE_FILE;
    return WRITE_OK;
}


void image_close(struct image *img) {
    if (img) {
        free(img->data);
        free(img);
    }
}


struct image *rotate(const struct image *const img) {
    struct image *rotated = malloc(sizeof(struct image));
    uint32_t x = 0, y = 0;
    rotated->data = malloc(sizeof(struct pixel) * img->width * img->height);
    rotated->width = img->height;
    rotated->height = img->width;
    for (x = 0; x < rotated->width; x++)
        for (y = 0; y < rotated->height; y++)
            *pixel_of(rotated, x, y) = *pixel_of(img, y, x);
    return rotated;
}

struct pixel *pixel_of(const struct image *const image, uint64_t x, uint64_t y) {
    return image->data + y * image->width + x;
}