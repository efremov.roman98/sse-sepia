#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdbool.h>
#include <string.h>
#include "image.h"

#define OUT_FILENAME "sepia.bmp"


int main(int argc, const char *argv[]) {
    struct image *img = NULL;
    const char *in_filename = NULL;
    bool use_c_sepia = true;
    enum read_status r_status;
    enum write_status w_status;
    struct rusage rusage;
    struct timeval start, end;
    long time_result;

    if (argc == 1) {
        printf("You need to enter a filename as argument!\n");
        return 1;
    }

    if (argc == 2) {
        in_filename = argv[1];
    } else if (argc == 3) {
        if (strcmp(argv[1], "--sse") == 0) {
            use_c_sepia = false;
            in_filename = argv[2];
        } else {
            printf("Unknown option: %s\n", argv[1]);
            return 1;
        }
    } else {
        printf("Too much arguments!\n");
        return 1;
    }

    r_status = image_open(in_filename, &img);
    if (READ_OK != r_status) {
        switch (r_status) {
            case READ_UNSUPPORTED_FORMAT:
                printf("Unsupported image format!\n");
                break;
            case READ_FAILED_TO_OPEN_FILE:
                printf("Failed to open image file!\n");
                break;
            case READ_FAILED_TO_CLOSE_FILE:
                printf("Failed to close image file!\n");
                break;
            case READ_INVALID_HEADER:
                printf("Invalid format header!\n");
                break;
            case READ_INVALID_BITS:
                printf("Invalid content of image!\n");
                break;
            case READ_OK:
                break;
        }
        return 1;
    }
    getrusage(RUSAGE_SELF, &rusage);
    start = rusage.ru_utime;

    if (use_c_sepia) {
        sepia_img(img);
    } else {
        sse_sepia_img(img);
    }
    getrusage(RUSAGE_SELF, &rusage);
    end = rusage.ru_utime;
    time_result = (end.tv_sec - start.tv_sec) * 1000000L + end.tv_usec - start.tv_usec;
    printf("Time elapsed in microseconds: %ld\n", time_result);

    w_status = image_write(OUT_FILENAME, img, BMP);

    if (WRITE_OK != w_status) {
        switch (w_status) {
            case WRITE_FAILED_TO_OPEN_FILE:
                printf("Failed to open %s for writing!\n", OUT_FILENAME);
                break;
            case WRITE_FAILED_TO_CLOSE_FILE:
                printf("Failed to close %s after writing!\n", OUT_FILENAME);
                break;
            case WRITE_FAILED_TO_WRITE:
                printf("Failed to write to %s!\n", OUT_FILENAME);
                break;
            case WRITE_UNSUPPORTED_FORMAT:
                printf("Unsupported output format!\n");
                break;
            case WRITE_FAILED_TO_CONVERT:
                printf("It is not possible to save this picture in this format!\n");
                break;
            case WRITE_OK:
                break;
        }
        image_close(img);
        return 1;
    }
    return 0;
}
