#ifndef SSE_SEPIA_H
#define SSE_SEPIA_H

#define USE_CRUTCHES


/**
 * Makes sepia filter of 4 pixels using SSE instructions
 * @param pixels array of 4 pixels. [i][0] - blue, [i][1] - green, [i][2] - red components of the i-th pixel
 */
extern void sse_sepia_4(uint8_t pixels[4][3]);


#ifdef USE_CRUTCHES

extern void crutch_sse_sepia_4(uint8_t pixels[4][3], const float CRUTCH_COEFFICIENTS[9][4]);

static const float CRUTCH_COEFFICIENTS[9][4] = {
        {.272f, .349f, .393f, .272f},
        {.543f, .686f, .769f, .543f},
        {.131f, .168f, .189f, .131f},

        {.349f, .393f, .272f, .349f},
        {.686f, .769f, .543f, .686f},
        {.168f, .189f, .131f, .168f},

        {.393f, .272f, .349f, .393f},
        {.769f, .543f, .686f, .769f},
        {.189f, .131f, .168f, .189f}
};


void sse_sepia_4(uint8_t pixels[4][3]) {
    crutch_sse_sepia_4(pixels, CRUTCH_COEFFICIENTS);
}

#endif /* USE_CRATCHES */

#endif /* SSE_SEPIA_H */