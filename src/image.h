#ifndef IMAGE_H
#define IMAGE_H

struct image;

#include "sepia.h"

enum image_format {
    BMP
};

enum read_status {
    READ_OK = 0,
    READ_FAILED_TO_OPEN_FILE,
    READ_FAILED_TO_CLOSE_FILE,
    READ_INVALID_HEADER,
    READ_UNSUPPORTED_FORMAT,
    READ_INVALID_BITS
};

enum write_status {
    WRITE_OK = 0,
    WRITE_FAILED_TO_OPEN_FILE,
    WRITE_FAILED_TO_CLOSE_FILE,
    WRITE_FAILED_TO_WRITE,
    WRITE_UNSUPPORTED_FORMAT,
    WRITE_FAILED_TO_CONVERT
};


extern enum read_status image_open(const char *const filename, struct image **const);

extern enum write_status image_write(const char *const filename, struct image *const, enum image_format);

extern void image_close(struct image *);

/* makes a rotated copy */
extern struct image *rotate(const struct image *const);

#endif
