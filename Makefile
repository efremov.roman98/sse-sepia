CC = gcc
ASMC = nasm
RM = rm
CFLAGS = -Wall -Werror -g -ansi -pedantic
ASMFLAGS = -felf64 -g 
BUILD_DIR = build
SRC_DIR = src
BIN_DIR = bin
FILES = image sepia sepia-test
OBJS = $(FILES:=.o)
ASM_FILES = sse_sepia
ASM_OBJS = $(ASM_FILES:=.o)
PROGRAM = sepia-test

all: $(ASM_OBJS) $(OBJS) .init-bin
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$(PROGRAM) $(addprefix $(BUILD_DIR)/, $(ASM_OBJS)) $(addprefix $(BUILD_DIR)/, $(OBJS)) 
	
.init-build:
	mkdir -p $(BUILD_DIR)

.init-bin:
	mkdir -p $(BIN_DIR)
 
$(OBJS): $(addprefix $(SRC_DIR)/, $(FILES:=.c)) .init-build
	$(CC) $(CFLAGS) -c $(SRC_DIR)/$(@:.o=.c) -o $(BUILD_DIR)/$(@)

$(ASM_OBJS): $(addprefix $(SRC_DIR)/, $(ASM_FILES:=.asm)) .init-build
	$(ASMC) $(ASMFLAGS) $(SRC_DIR)/$(@:.o=.asm) -o $(BUILD_DIR)/$(@)

clean:
	$(RM) -rf $(BUILD_DIR)
